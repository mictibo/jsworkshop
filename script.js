//// Hello world to open the app
// nw.Window.get().showDevTools();

// var os = nw.require("os");
// console.log(os.type);

// $(document).ready(function() {

//     $("body").append(os.hostname);

// });

// chrome.tts.speak("Hello all");



////Making a button and flashing the light
// nw.require("nwjs-j5-fix").fix();
// nw.Window.get().showDevTools();
// var five = nw.require("johnny-five");
// var board = new five.Board({port: "COM3"});
// board.on("ready", function() {
//     console.log("Yeah!");

//     var pin = 13;
//         interval = 250;

//     var led = new five.Led(pin);

//     led.blink(interval);

//     QuickSettings.create(100, 200, "LED Settings")
//                  .addRange("Interval", 50, 500, 250, 5, function(value) {
//                     interval = value;
//                     led.blink(interval);
//                  })
//                  .addButton("Toggle", function() {
//                     if (led.isRunning) {
//                         led.stop().off();
//                     } else {
//                         led.blink(interval);
//                     }
//                  });

// });


////Going through space activity !
// nw.require("nwjs-j5-fix").fix();
// nw.Window.get().showDevTools();

// var five = nw.require("johnny-five");
// var board = new five.Board({port: "COM3"});

// window.addEventListener("DOMContentLoaded", function() {

//     var canvas = document.getElementsByTagName('canvas')[0];
//     var context = canvas.getContext('2d');
//     var particles = [];

//     canvas.width = window.innerWidth;
//     canvas.height = window.innerHeight;

//     var settings = QuickSettings.create(100, 100, "Particles Settings")
//                                 .addRange("size", 0, 10, 1, 0.1)
//                                 .addColor("color", "#FFFFFF");

    
//     board.on("ready", function() {
//         console.log("Yeah!");

//         var led = new five.Led(13);
//         led.on();

//         var proximity = new five.Proximity({
//             controller:"2Y0A21",
//             pin: "A0"
//         });

//         proximity.within([8, 65] ,"cm", function() {
//             console.log(this.cm);

//             var proximitySquared = Math.pow(65 - this.cm, 2);
//             var count = proximitySquared / 100;
//             var size = proximitySquared / 1000;
//             var speed = proximitySquared / 50;

//             for (var i = 0; i < count ; i++) {
//                 var particle = new Particle({
//                     x: canvas.width/2,
//                     y: canvas.height/2,
//                     radius: size * settings.getNumberValue("size"),
//                     speed: speed,
//                     color: settings.getColor("color")
//                 });

//                 particles.push(particle);
//             }
//         });

//     });

//     Particle.startRendering(context, particles);
// });



// // Theremin
// nw.require("nwjs-j5-fix").fix();
// nw.Window.get().showDevTools();

// var five = nw.require("johnny-five");
// var board = new five.Board({port: "COM3"});

// window.addEventListener("DOMContentLoaded", function() {

//     var osc = new Tone.Oscillator(440, "sine").toMaster();
//     osc.start();

//     board.on("ready", function() {
//         console.log("Yeah!");

//         var led = new five.Led(13);
//         led.on();

//         var proximity = new five.Proximity({
//             controller:"2Y0A21",
//             pin: "A0"
//         });

//         setInterval(function () {
//             if (proximity.cm > 8 && proximity.cm < 65) {
//                 if (osc.volume.value <= -Infinity) {
//                     osc.volume.rampTo(-6, 0.02);
//                 } else {
//                     osc.frequency.rampTo(proximity.cm * 20, 0.1);
//                 }
//             } else if (osc.volume.value > -Infinity) {
//                 osc.frequency.rampTo(-Infinity, 1);
//             }
//         }, 25);

//     });

// });

// // Random servo motor
// nw.require("nwjs-j5-fix").fix();
// nw.Window.get().showDevTools();

// var five = nw.require("johnny-five");
// var board = new five.Board({port: "COM3"});

// window.addEventListener("DOMContentLoaded", function() {

//     board.on("ready", function() {
//         console.log("Yeah!");

//         var servo = new five.Servo({
//             pin:10,
//             center: true
//         });

//         document.querySelector("input").addEventListener('keyup', function(e) {
//             if (e.keyCode !== 13) return;

//             var positions = {
//                 "yes": 25,
//                 "maybe": 90,
//                 "no": 150
//             };

//             var answer = ["yes", "maybe", "no"][Math.floor(Math.random() * 3)];

//             servo.sweep();
//             setTimeout(function() {
//                 servo.stop();
//                 servo.to(positions[answer]);
//             }, 3000);
//         });

//     });

// });


nw.require("nwjs-j5-fix").fix();
nw.Window.get().showDevTools();

var five = nw.require("johnny-five");
var board = new five.Board({port: "COM3"});

window.addEventListener("DOMContentLoaded", function() {

    board.on("ready", function() {
        console.log("Yeah!");

        var led = new five.Led.RGB({
            pins: {
                red: 3,
                blue: 5,
                green: 6
            }
        });

        var settings = QuickSettings.create(100, 100, "Settings")
            .addRange("intensity", 0, 100, 50, 1, function() {
                led.intensity(settings.getRangeValue("intensity"));
            })
            .addColor("color", "#FFFFFF", function() {
                led.color(settings.getColor("color"));
            })
            .addButton("toggle", function(){
                led.toggle();
            });

            var image = document.querySelector('img');
            var canvas = document.querySelector('canvas');
            var context = canvas.getContext("2d");

            canvas.width = window.innerWidth;
            canvas.height = window.innerHeight;
            context.drawImage(image, 0, 0, image.width, image.height);

            canvas.addEventListener("mousemove", function(e) {
                let [r, g, b, a] = context.getImageData(e.clientX, e.clientY, 1, 1).data;
                led.color({
                    red: r,
                    green: g,
                    blue: b
                });
            });
    });

});